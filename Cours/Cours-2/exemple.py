#!/bin/python3

import numpy as np
import scipy.linalg as nla

L = np.array ([[2, 0, 0], [3, 1, 0], [2, -1, 2]], dtype=np.float64, order='F')
U = np.array ([[3, 0, 1], [0, 2, -4], [0, 0, 5]], dtype=np.float64, order='F')

A = np.dot (L, U)

# On triche. On part de x. On calcule b. But du jeu: retrouver x à partir de b

x = np.array ([1,2,3], dtype=np.float64)
b = np.dot (A, x)

# 1. On calcule y
y = nla.solve_triangular (L, b, lower=True)
# 2. On calcule x (qu'on note xbar ici)
xbar = nla.solve_triangular (U, y, lower=False)

# On recommence de façon plus efficace
nla.solve_triangular (L, b, lower=True, overwrite_b=True)
nla.solve_triangular (U, b, lower=False, overwrite_b=True)

